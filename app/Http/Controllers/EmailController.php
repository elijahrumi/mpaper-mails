<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmailNotification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class EmailController extends Controller
{
    
    public function mails(Request $request){
        //For testing purposes


        $pending_mails = EmailNotification::where('email_status',2)->get();
        dd($pending_mails);
        $sent_mails = array();
        foreach ($pending_mails as $key => $mail) {

            try {

                // Mail::send([], [], function ($message) use ($mail){
                //   $message->to('benjamini@smartcodes.co.tz')
                //     ->subject($mail->email_subject)
                //     ->setBody($mail->email_message, 'text/html'); // for HTML rich messages
                // });
                
                Log::channel('mails')->info('Email sent to '.$mail->email_recipient);
                $sent_mails[] = $mail->email_notification_id;

            } catch (Exception $ex) {
                // Debug via $ex->getMessage();
                Log::channel('mails')->info('Email Sending Failure');
            }
        }

        dd($sent_mails);
        EmailNotification::whereIn('email_notification_id',$sent_mails)->update(['email_status' => 1]);
    }
}


