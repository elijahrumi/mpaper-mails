<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\EmailNotification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class MailSender extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email Notifications from the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if (Storage::exists('mails.lock')) {
            Log::channel('mails')->info('The Lock is on');

            $this->info('The Lock is on');
            exit();
        }

        Storage::put('mails.lock', 'Just a lock file');


        $pending_mails = EmailNotification::where('email_status',2)->get();
        //dd($pending_mails);
        $sent_mails = array();
        foreach ($pending_mails as $key => $mail) {

            try {

                Mail::send([], [], function ($message) use ($mail){
                  $message->to($mail->email_recipient)
                    ->subject($mail->email_subject)
                    ->setBody($mail->email_message, 'text/html'); // for HTML rich messages
                });
                
                Log::channel('mails')->info('Email sent to '.$mail->email_recipient);
                $sent_mails[] = $mail->email_notification_id;
                $mail->email_status = 1;
                $mail->save();

            } catch (Exception $ex) {
                // Debug via $ex->getMessage();
                Log::channel('mails')->info('Email Sending Failure');
            }
        }

        Log::channel('mails')->info('Send:Mails Command Run successfully, Sent '.count($sent_mails).' out of ' .count($pending_mails).' mails');

        $this->info('Send:Mails Command Run successfully, Sent '.count($sent_mails).' out of ' .count($pending_mails).' mails');

        Storage::delete('mails.lock');
    }
}
