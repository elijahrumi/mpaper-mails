<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailNotification extends Model
{
    protected $primaryKey = 'email_notification_id';
    protected $table = 'email_notifications';
}
